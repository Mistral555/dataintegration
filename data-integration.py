import pandas as pd
import psycopg2
from dotenv import load_dotenv
import os
import json
import xml.etree.ElementTree as ET

# Chargement des variable d'environnement
load_dotenv()
db_driver = os.getenv("DB_DRIVER")
db_host = os.getenv("DB_HOST")
db_port = os.getenv("DB_PORT")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
db_name = os.getenv("DB_NAME")

# Connexion à la base de données
connection_string = f"{db_driver}://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"

try:
    connection = psycopg2.connect(connection_string)
    cursor = connection.cursor()
    cursor.execute("SELECT version();")
    db_version = cursor.fetchone()
    print("Connected to:", db_version)
    cursor.close()
    connection.close()
except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL:", error)



# Load the CSV data
csv_file_path = './data/CSV_0_10000.csv'  # Update this path
df = pd.read_csv(csv_file_path, sep=';' , encoding='latin-1')

# Create and connect to the database
conn = psycopg2.connect(connection_string)
cursor = conn.cursor()

# Create tables
cursor.execute("""
    CREATE TABLE IF NOT EXISTS Country (
        Country_ID SERIAL PRIMARY KEY,
        Country_Name VARCHAR(255) UNIQUE
    );
""")

cursor.execute("""
    CREATE TABLE IF NOT EXISTS Year (
        Year_ID SERIAL PRIMARY KEY,
        Year INT UNIQUE
    );
""")

cursor.execute("""
    CREATE TABLE IF NOT EXISTS Obesity_Rate (
        Obesity_ID SERIAL PRIMARY KEY,
        Country_ID INT,
        Year_ID INT,
        Obesity_Rate FLOAT,
        Sex VARCHAR(50),
        FOREIGN KEY (Country_ID) REFERENCES Country (Country_ID),
        FOREIGN KEY (Year_ID) REFERENCES Year (Year_ID)
    );
""")

conn.commit()

# Insert countries
for country in df['Country'].unique():
    cursor.execute("INSERT INTO Country (Country_Name) VALUES (%s) ON CONFLICT (Country_Name) DO NOTHING;", (country,))
conn.commit()


# Insert years
for year in df['Year'].unique():
    year_value = int(year)  # Convert numpy.int64 to int
    cursor.execute("INSERT INTO Year (Year) VALUES (%s) ON CONFLICT (Year) DO NOTHING;", (year_value,))
conn.commit()

# Insert obesity rates
for _, row in df.iterrows():
    cursor.execute("SELECT Country_ID FROM Country WHERE Country_Name = %s;", (row['Country'],))
    country_id = cursor.fetchone()[0]
    
    cursor.execute("SELECT Year_ID FROM Year WHERE Year = %s;", (row['Year'],))
    year_id = cursor.fetchone()[0]
    
    cursor.execute("""
        INSERT INTO Obesity_Rate (Country_ID, Year_ID, Obesity_Rate, Sex) VALUES (%s, %s, %s, %s);
    """, (country_id, year_id, row['Obesity (%)'], row['Sex']))

conn.commit()

json_file_path = './data/json_20000_fin.json'  # Update this path
with open(json_file_path, 'r') as file:
    json_data = json.load(file)

# Insert countries from JSON
countries = set(entry['Country'] for entry in json_data)  # Create a set of unique countries
for country in countries:
    cursor.execute("INSERT INTO Country (Country_Name) VALUES (%s) ON CONFLICT (Country_Name) DO NOTHING;", (country,))
conn.commit()

# Insert years from JSON
years = set(entry['Year'] for entry in json_data)  # Create a set of unique years
for year in years:
    year_value = int(year)  # Ensure year is an integer
    cursor.execute("INSERT INTO Year (Year) VALUES (%s) ON CONFLICT (Year) DO NOTHING;", (year_value,))
conn.commit()

# Insert obesity rates from JSON
for entry in json_data:
    cursor.execute("SELECT Country_ID FROM Country WHERE Country_Name = %s;", (entry['Country'],))
    country_id = cursor.fetchone()[0]
    
    cursor.execute("SELECT Year_ID FROM Year WHERE Year = %s;", (entry['Year'],))
    year_id = cursor.fetchone()[0]
    
    cursor.execute("""
        INSERT INTO Obesity_Rate (Country_ID, Year_ID, Obesity_Rate, Sex) VALUES (%s, %s, %s, %s);
    """, (country_id, year_id, entry['Obesity (%)'], entry['Sex']))

conn.commit()

# Chemin d'accès au fichier XML

# Chemin vers votre fichier XML
xml_file_path = './data/xml_10000_20000.xml'  # Mettez à jour ce chemin

# Parsez le fichier XML
tree = ET.parse(xml_file_path)
root = tree.getroot()

# Insérez les données dans les tables
for entry in root.findall('row'):  # Remplacez 'record' par le bon élément qui représente une entrée de données dans votre fichier XML
    country = entry.find('Country').text
    year = int(entry.find('Year').text)
    obesity_rate = float(entry.find('Obesity').text.replace(',', '.'))  # Assurez-vous que le taux est bien un float
    sex = entry.find('Sex').text

    # Insérez ou ignorez le pays si déjà présent
    cursor.execute("INSERT INTO Country (Country_Name) VALUES (%s) ON CONFLICT (Country_Name) DO NOTHING;", (country,))
    conn.commit()

    # Insérez ou ignorez l'année si déjà présente
    cursor.execute("INSERT INTO Year (Year) VALUES (%s) ON CONFLICT (Year) DO NOTHING;", (year,))
    conn.commit()

    # Récupérez les IDs pour country et year
    cursor.execute("SELECT Country_ID FROM Country WHERE Country_Name = %s;", (country,))
    country_id = cursor.fetchone()[0]

    cursor.execute("SELECT Year_ID FROM Year WHERE Year = %s;", (year,))
    year_id = cursor.fetchone()[0]

    # Insérez les données de taux d'obésité
    cursor.execute("""
        INSERT INTO Obesity_Rate (Country_ID, Year_ID, Obesity_Rate, Sex) VALUES (%s, %s, %s, %s);
    """, (country_id, year_id, obesity_rate, sex))
    conn.commit()
# Close the connection
cursor.close()
conn.close()

print("Data integrated successfully into the database.")
